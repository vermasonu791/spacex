# SpacEx

### Project Description

created a Server Side Rendered React App

ES2015.: A style of Javascript used by ReactJS
CommonJS: The style of Javascript used by your browser (?)
Babel: Turns your ReactJS code (ES2015~) into CommonJS (transpile)
Webpack: Packs everything into a single js file that can be imported AFTER server side rendering is finished
Express: Just another webserver

### File structure

     |-- build
        |-- bundle.jss
        |-- main.css
     |-- public
        |-- bundle.jss
        |-- main.css
     |-- .gitignore
     |-- package-lock.json
     |-- package.json
     |-- webpack.base.js
     |-- webpack.client.js
     |-- webpack.server.js
     |-- src
        |--client
            |-- server.js
            |-- client.js
            |-- actions
            |   |-- index.js
            |-- components
            |   |-- filter.js
            |   |-- renderFilterData.jsx
            |   |-- radioButton.js
            |-- helper
            |   |-- createStore.js
            |   |-- render.js
            |-- reducers
            |   |-- index.js
            |   |-- launchProgramReducer.js
            |-- routes
            |    |-- index.js
            |-- styles
            |    |-- components
            |    |   |--filter.css
            |    |   |--filterData.css
            |    |   |--home.css
            |    |-- global.css
            |-- views
                    |-- index.ejs

### clone the directory

1. git clone https://vermasonu791@bitbucket.org/vermasonu791/spacex.git

2. cd spacex

### After clone the project run the further command in terminal

1. install all packages `npm install`

2. `npm run build`

3. `npm start`

4. open `http://localhost:3000/`
