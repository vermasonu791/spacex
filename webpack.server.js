const path = require("path");
const merge = require("webpack-merge");
const baseConfig = require("./webpack.base.js");
const nodeExternals = require("webpack-node-externals");
const config = {
  target: "node",
  entry: "./src/server.js",

  output: {
    filename: "bundle.js",
    path: path.resolve(__dirname, "build")
  },
  externals: [nodeExternals()]
};
module.exports = merge(baseConfig, config);
