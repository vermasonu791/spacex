import "babel-polyfill";
import express from "express";
import render from "./client/helpers/render";
import createStore from "./client/helpers/createStore";

// Server var
const app = express();

// Middleware
app.use(express.static("public"));

app.get("*", (req, res) => {
  const store = createStore();
  res.send(render(req, store));
});

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log("Listening on Port 3000");
});
