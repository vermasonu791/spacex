import React from "react";
import { renderToString } from "react-dom/server";
import { StaticRouter } from "react-router-dom";
import Routes from "../routes/index";
import { Provider } from "react-redux";

export default (req, store) => {
  const content = renderToString(
    <Provider store={store}>
      <StaticRouter location={req.path} context={{}}>
        <Routes />
      </StaticRouter>
    </Provider>
  );
  return `
        <!DOCTYPE html>
        <html>
            <head>
            <title>SpaceX</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta property="og:title" content="SSR REACT">
            <meta property="og:description" content="React , Html, css, javascript">
            <meta property="og:image" content="https://www.google.com/imgres?imgurl=https%3A%2F%2Fcreate-react-app.dev%2Fimg%2Flogo-og.png&imgrefurl=https%3A%2F%2Fcreate-react-app.dev%2Fdocs%2Fadding-images-fonts-and-files%2F&tbnid=RFpj5bRBm2UJmM&vet=12ahUKEwjaibmvxsLrAhWQNisKHVyEADAQMygAegUIARCsAQ..i&docid=sSxSMWrLW5TTQM&w=1200&h=630&q=react%20image&ved=2ahUKEwjaibmvxsLrAhWQNisKHVyEADAQMygAegUIARCsAQ">
            <meta name="author" content="sonu verma">
            <link rel="stylesheet" href="main.css">
            </head>
            <body>
                <div id="root">${content}</div>
                <script src="bundle.js"></script>
            </body>
        </html>
    `;
};
