import React from "react";
import { connect } from "react-redux";
import { launchProgram } from "../actions";
import "../styles/component/home.css";
import "../styles/global.css";
import Filter from "../component/filter";
import FilterData from "../component/renderFilterData";
import { useState, useEffect } from "react";

const Home = props => {
  const [passParam, setParm] = useState({ limit: 100 });
  useEffect(() => {
    props.getLaunchData(passParam);
  }, [passParam]);

  // callback function to get selected filter data as object
  let getParam = newParam => {
    setParm(prevState => ({
      ...prevState,
      ...newParam
    }));
  };

  return (
    <div className="laout_container">
      <div>
        <h2>SpacEx Launch program</h2>
      </div>
      <div className="parent-container">
        <div className="filter">
          <Filter sendParam={getParam} />
        </div>
        <div className="programs-data">
          <FilterData cardData={props.list} />
        </div>
      </div>
      <div className="devloped-by">
        <h3>Developed By:&nbsp;</h3>
        <p>Sonu Verma</p>
      </div>
    </div>
  );
};

function mapStateToProps(state) {
  return { list: state.programs.program };
}

function mapDispatchToProps(dispatch) {
  return {
    getLaunchData: passParam => dispatch(launchProgram(passParam))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
