import React from "react";

const radioButton = props => {
  return (
    <div className="radio-button">
      <input
        id={props.id}
        onChange={e => {
          props.changed(e);
        }}
        value={props.value}
        type="radio"
        checked={props.isSelected}
        name={props.name}
      />
      <label htmlFor={props.id} className={props.selected}>
        {props.value}
      </label>
    </div>
  );
};

export default radioButton;
