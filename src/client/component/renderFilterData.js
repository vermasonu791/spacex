import * as React from "react";
import "../styles/component/filterData.css";

const FilterData = props => {
  return (
    <div className="filter-data">
      <div className="card-container">
        {props.cardData.map((data, index) => {
          return (
            <div className="card" key={index}>
              <div className="img-container"></div>
              <div className="mission-name bold-text">
                {data.mission_name} #{data.flight_number}
              </div>
              <div className="mission-id">
                <h4>Mission Ids:</h4>
                <ul>
                  {data.mission_id.map((list, index) => {
                    return <li key={index}>{list}</li>;
                  })}
                </ul>
              </div>
              <div className="launch-heading">
                <span className="bold-text ">Launch Year:</span>
                {data.launch_year}
              </div>
              <div className="launch-heading">
                <span className="bold-text ">Successful Launch:</span>
                {data.launch_success == true ? "true" : "false"}
              </div>
              <div>
                <span className="bold-text ">Successful Landing:</span>
                {data.rocket.first_stage.cores[0].land_success == true
                  ? "true"
                  : "false"}
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default FilterData;
