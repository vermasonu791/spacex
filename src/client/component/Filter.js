import * as React from "react";
import "../styles/component/filter.css";
import RadioButton from "./radioButton";
import { useState, useEffect } from "react";

const Filter = props => {
  const [launchYear, setLaunchYear] = useState("");
  const [successLaunch, setSuccessLaunch] = useState(false);
  const [successLaing, setSuccessLand] = useState(false);
  const [paramObj, setState] = useState({});

  let getLaunchValue = e => {
    setLaunchYear(e.target.value);
    const value = e.target.value;
    setState(prevState => ({
      ...prevState,
      launch_year: parseInt(value)
    }));
  };

  let getSuccessLaunchValue = e => {
    setSuccessLaunch(e.target.value);
    const value = e.target.value;
    setState(prevState => ({
      ...prevState,
      launch_success: value == "true" ? true : false
    }));
  };

  let getSuccessLandValue = e => {
    setSuccessLand(e.target.value);
    const value = e.target.value;
    setState(prevState => ({
      ...prevState,
      land_success: value == "true" ? true : false
    }));
  };
  useEffect(() => {
    props.sendParam(paramObj);
  }, [paramObj]);

  return (
    <div className="filter-box">
      <div className="filter-heading">
        <h3>Filters</h3>
      </div>
      <div className="filter-buttons">
        <p>Launch Year</p>
        <div className="hr-line"></div>
        <div className="radio-container">
          {staticLaunchYear.map((data, index) => {
            return (
              <div className="radio-child" key={index}>
                <RadioButton
                  changed={e => {
                    getLaunchValue(e);
                  }}
                  id={data}
                  isSelected={launchYear === data}
                  value={data}
                  selected={launchYear === data ? "selected" : ""}
                  name={"radio-field"}
                />
              </div>
            );
          })}
        </div>
        <p>Successful Launch</p>
        <div className="hr-line"></div>
        <div className="radio-container">
          {sucessLaunch.map((data, index) => {
            return (
              <div className="radio-child" key={index}>
                <RadioButton
                  changed={e => {
                    getSuccessLaunchValue(e);
                  }}
                  id={data.id}
                  isSelected={successLaunch == data.value}
                  value={data.value}
                  selected={successLaunch == data.value ? "selected" : ""}
                  name={"radio-field"}
                />
              </div>
            );
          })}
        </div>
        <p>Successful Landing</p>
        <div className="hr-line"></div>
        <div className="radio-container">
          {successLanding.map((data, index) => {
            return (
              <div className="radio-child" key={index}>
                <RadioButton
                  changed={e => {
                    getSuccessLandValue(e);
                  }}
                  id={data.id}
                  isSelected={successLaing == data.value}
                  value={data.value}
                  selected={successLaing == data.value ? "selected" : ""}
                  name={"radio-field"}
                />
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default Filter;

const staticLaunchYear = [
  "2006",
  "2007",
  "2008",
  "2009",
  "2010",
  "2011",
  "2012",
  "2013",
  "2014",
  "2015",
  "2016",
  "2017",
  "2018",
  "2019",
  "2020"
];
const sucessLaunch = [
  { id: "success-1", value: "true" },
  { id: "success-2", value: "false" }
];

const successLanding = [
  { id: "success-laing-1", value: "true" },
  { id: "success-laing-2", value: "false" }
];
