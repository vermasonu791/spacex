import { combineReducers } from "redux";
import launchProgramReducer from "./launchProgramReducer";

const rootReducer = combineReducers({
  programs: launchProgramReducer
});

export default rootReducer;
