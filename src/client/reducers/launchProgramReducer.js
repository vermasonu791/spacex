import { LAUNCH_PROGRAMS } from "../actions";

export default (state = { program: [] }, action) => {
  switch (action.type) {
    case LAUNCH_PROGRAMS:
      return { ...state, program: action.payload };
    default:
      return state;
  }
};
