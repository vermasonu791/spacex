import React from "react";
import { Route } from "react-router-dom";
import Home from "../views/index";

export default () => {
  return (
    <div>
      <Route exact path="/" render={() => <Home />} />
    </div>
  );
};
