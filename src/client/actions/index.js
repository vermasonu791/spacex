import axios from "axios";

export const LAUNCH_PROGRAMS = "LAUNCH_PROGRAMS";

export const launchProgram = paramsObj => async dispatch => {
  const res = await axios.get("https://api.spacexdata.com/v3/launches", {
    params: paramsObj
  });
  dispatch({
    type: LAUNCH_PROGRAMS,
    payload: res.data
  });
};
